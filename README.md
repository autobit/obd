Autobit On-board Diagnostics
===

The Autobit On-board Diagnostics project reads diagnostic data from vehicles using the standard OBDII interface. It is released under the MIT license. This project targets Java 1.7, so it is compatible with Android. This project can communicate with the ELM327 IC out of the box.

# Usage (Gradle)

First, add the following to your `repositories` block:

```
maven {
  url 'https://api.bitbucket.org/1.0/repositories/autobit/obd/raw/releases'
}
```

Optionally, to use snapshot releases, you can also add the following:

```
maven {
  url 'https://api.bitbucket.org/1.0/repositories/autobit/obd/raw/snapshots'
}
```

Now you can bring in the dependency like normal:

```
compile 'ca.autobit:obd:2.1.3'
```

# Getting Started

If you're working with a serial device on Linux, you'll likely do fine using the built-in `Elm327SerialConnector` class. To use the `Elm327SerialConnector`, pass the device name into the constructor (typically `/dev/ttyUSB0`). Once you've constructed a connector instance, you can create `Pid` objects to send commands to your vehicle.

## Connector Objects

Communicating with the vehicle is done using a `Connector` (for example, the `Elm327SerialConnector` above). These objects contain the know-how for sending and receiving data in a specific format.

For example: if you plan on communicating over Bluetooth, you should create a `BluetoothConnector` and implement the `Connector` interface.

With all connectors, the first call after constructing an instance is typically to the `connect` method. The utilities provided in the library that require a connector assume that the connector instance has been initialized by the calling program by calling `connect`.

## Response Parsers

A `ResponseParser` is an object that is used to parse the responses from the vehicle. These can be found in the `ca.autobit.obd.parser` package. You should investigate the files to find out what can be done, but the typical usage of a parser is:

```java
Connector connector = // get an instance to a connector
VehicleSpeedParser speedParser = new VehicleSpeedParser();
connector.connect();
int speed = speedParser.parse(connector.request(Pid.VEHICLE_SPEED));

System.out.println("Vehicle speed: " + speed + " km/h");
```

# Help & Support

Please be aware that this software is provided as-is without any guarantees. If you have a problem, please provide details (preferably include a Gradle Build Scan if possible). Forking and contributions are welcome!
