package ca.autobit.obd.util;

import ca.autobit.obd.Pid;
import ca.autobit.obd.parser.*;
import org.junit.Test;

import javax.swing.text.html.parser.Parser;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class ResponseParserRegestryTests {
  @Test
  public void testGetResponseParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ACTUAL_TORQUE);

    assertThat(parser.getClass().toString(), equalTo(ActualTorqueParser.class.toString()));
  }
  
  @Test 
  public void testGetAbsoluteFuelRailPressureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.FUEL_RAIL_ABSOLUTE_PRESSURE);
    
    assertThat(parser.getClass().toString(), equalTo(AbsoluteFuelRailPressureParser.class.toString()));
  }
  
  @Test 
  public void testGetBatteryVoltageParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.BATTERY_VOLTAGE);
    
    assertThat(parser.getClass().toString(), equalTo(BatteryVoltageParser.class.toString()));
  }
  
  @Test 
  public void testGetDemandTorqueParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.DEMAND_TORQUE);
    
    assertThat(parser.getClass().toString(), equalTo(DemandTorqueParser.class.toString()));
  }
  
  @Test 
  public void testGetDistanceSinceCodesClearedParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.DISTANCE_TRAVELED_SINCE_CODES_CLEARED);
    
    assertThat(parser.getClass().toString(), equalTo(DistanceSinceCodesClearedParser.class.toString()));
  }
  
  @Test 
  public void testGetEngineCoolantTemperatureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ENGINE_COOLANT_TEMPERATURE);
    
    assertThat(parser.getClass().toString(), equalTo(EngineCoolantTemperatureParser.class.toString()));
  }
  
  @Test 
  public void testGetEngineLoadParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.CALCULATED_ENGINE_LOAD);
    
    assertThat(parser.getClass().toString(), equalTo(EngineLoadParser.class.toString()));
  }
  
  @Test 
  public void testGetEngineRpmParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ENGINE_RPM);
    
    assertThat(parser.getClass().toString(), equalTo(EngineRpmParser.class.toString()));
  }
  
  @Test 
  public void testGetEngineRunTimeParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ENGINE_RUN_TIME);
    
    assertThat(parser.getClass().toString(), equalTo(EngineRunTimeParser.class.toString()));
  }
  
  @Test 
  public void testGetEvaporationVaporPressureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.EVAPORATION_SYSTEM_VAPOR_PRESSURE);
    
    assertThat(parser.getClass().toString(), equalTo(EvaporationVaporPressureParser.class.toString()));
  }
  
  @Test 
  public void testGetFuelInjectionRateParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ENGINE_FUEL_RATE);
    
    assertThat(parser.getClass().toString(), equalTo(FuelInjectionRateParser.class.toString()));
  }
  
  @Test 
  public void testGetFuelInjectionTimingParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.FUEL_INJECTION_TIMING);
    
    assertThat(parser.getClass().toString(), equalTo(FuelInjectionTimingParser.class.toString()));
  }
  
  @Test 
  public void testGetFuelLevelInputParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.FUEL_TANK_LEVEL_INPUT);
    
    assertThat(parser.getClass().toString(), equalTo(FuelLevelInputParser.class.toString()));
  }
  
  @Test 
  public void testGetFuelRailPressureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.FUEL_RAIL_PRESSURE);
    
    assertThat(parser.getClass().toString(), equalTo(FuelPressureParser.class.toString()));
  }
  
  @Test 
  public void testGetFuelGuagePressureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.FUEL_GAUGE_PRESSURE);
    
    assertThat(parser.getClass().toString(), equalTo(FuelPressureParser.class.toString()));
  }
  
  @Test 
  public void testGetHybridBatteryRemainingLifeParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.HYBRID_BATTERY_PACK_REMAINING_LIFE);
    
    assertThat(parser.getClass().toString(), equalTo(HybridBatteryRemainingLifeParser.class.toString()));
  }
  
  @Test 
  public void testGetIgnitionTimingAdvanceParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.TIMING_ADVANCE);
    
    assertThat(parser.getClass().toString(), equalTo(IgnitionTimingAdvanceParser.class.toString()));
  }
  
  @Test 
  public void testGetIntakeAirTemperatureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.INTAKE_AIR_TEMPERATURE);
    
    assertThat(parser.getClass().toString(), equalTo(IntakeAirTemperatureParser.class.toString()));
  }
  
  @Test 
  public void testGetIntakeManifoldAbsolutePressureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.INTAKE_MANIFOLD_ABSOLUTE_PRESSURE);
    
    assertThat(parser.getClass().toString(), equalTo(IntakeManifoldAbsolutePressureParser.class.toString()));
  }
  
  @Test 
  public void testGetMassAirFlowRateParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.MAF_AIR_FLOW_RATE);
    
    assertThat(parser.getClass().toString(), equalTo(MassAirFlowRateParser.class.toString()));
  }
  
  @Test 
  public void testGetMonitorDtcStatusParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.MONITOR_STATUS_SINCE_DTCS_CLEARED);
    
    assertThat(parser.getClass().toString(), equalTo(MonitorStatusParser.class.toString()));
  }
  
  @Test 
  public void testGetMonitorThisDriveStatusParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.MONITOR_STATUS_THIS_DRIVE_CYCLE);
    
    assertThat(parser.getClass().toString(), equalTo(MonitorStatusParser.class.toString()));
  }
  
  @Test 
  public void testGetOilTemperatureParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ENGINE_OIL_TEMPERATURE);
    
    assertThat(parser.getClass().toString(), equalTo(OilTemperatureParser.class.toString()));
  }
  
  @Test 
  public void testGetPendingTroubleCodeParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PENDING_DTCS);
    
    assertThat(parser.getClass().toString(), equalTo(PendingTroubleCodeParser.class.toString()));
  }
  
  @Test 
  public void testGetPermanentTroubleCodeParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PERMANENT_DTCS);
    
    assertThat(parser.getClass().toString(), equalTo(PermanentTroubleCodeParser.class.toString()));
  }
  
  @Test 
  public void testGetReferenceTorqueParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.ENGINE_REFERENCE_TORQUE);
    
    assertThat(parser.getClass().toString(), equalTo(ReferenceTorqueParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParser0120() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_01_20);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParser2140() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_21_40);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParser4160() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_41_60);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParser6180() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_61_80);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParser81A0() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_81_A0);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParserA1C0() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_A1_C0);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetSupportedPidParserC1E0() {
    ResponseParser parser = ParserRegistry.getParser(Pid.PIDS_SUPPORTED_C1_E0);
    
    assertThat(parser.getClass().toString(), equalTo(SupportedPidParser.class.toString()));
  }
  
  @Test 
  public void testGetThrottlePositionParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.THROTTLE_POSITION);
    
    assertThat(parser.getClass().toString(), equalTo(ThrottlePositionParser.class.toString()));
  }
  
  @Test 
  public void testGetTroubleCodeParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.STORED_DTCS);
    
    assertThat(parser.getClass().toString(), equalTo(TroubleCodeParser.class.toString()));
  }
  
  @Test 
  public void testGetVehicleSpeedParser() {
    ResponseParser parser = ParserRegistry.getParser(Pid.VEHICLE_SPEED);
    
    assertThat(parser.getClass().toString(), equalTo(VehicleSpeedParser.class.toString()));
  }

  @Test(expected = CannotFindParserException.class)
  public void testGetUnRegisterParser() {
    ParserRegistry.getParser(new Pid(Integer.MAX_VALUE));
  }
}
