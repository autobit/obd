package ca.autobit.obd.util;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class StringUtilTests {
  private static final StringUtil stringUtil = StringUtil.getInstance();

  @Test
  public void testGetLineCountWithNullString() {
    int lines = stringUtil.getLineCount(null);

    assertThat(lines, equalTo(0));
  }

  @Test
  public void testGetLineCount() {
    String test = "\n\n\n\rHello\rWorld\r\r\n";
    int lines = stringUtil.getLineCount(test);

    assertThat(lines, equalTo(2));
  }

  @Test
  public void testGetLineCountWithEmptyString() {
    int lines = stringUtil.getLineCount("");

    assertThat(lines, equalTo(0));
  }

  @Test
  public void testStripAlphaNumericWithSpaces() {
    String test = "$$$hello \r\nworld%^@";
    String expected = "hello world";
    String actual = stringUtil.stripNonAlphaNumeric(test, false);

    assertThat(actual, equalTo(expected));
  }

  @Test
  public void testStripNonAlphaNumericWithoutSpaces() {
    String test = "$$$hello \r\nworld%^@";
    String expected = "helloworld";
    String actual = stringUtil.stripNonAlphaNumeric(test, true);

    assertThat(actual, equalTo(expected));
  }

  @Test
  public void testStripNewLines() {
    String test = "\r\n\r\r\r\nhello\r\nworld\r\r\n\n";
    String expected = "helloworld";
    String actual = stringUtil.stripNewLines(test);

    assertThat(actual, equalTo(expected));
  }
}
