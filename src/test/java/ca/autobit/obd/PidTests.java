package ca.autobit.obd;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class PidTests {
  @Test
  public void testPidEquals() {
    assertThat(new Pid("01 02"), equalTo(new Pid("0102")));
    assertThat(new Pid("0102"), equalTo(new Pid(0x0102)));
  }

  @Test
  public void testHashCode() {
    assertThat(new Pid("0100").hashCode(), equalTo("0100".hashCode()));
  }
}
