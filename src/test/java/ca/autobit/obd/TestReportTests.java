package ca.autobit.obd;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.core.IsEqual.equalTo;

public class TestReportTests {
  @Test
  public void testReport() {
    List<TestResult> testResults = new ArrayList<>();
    testResults.add(new TestResult(new Pid(0x0001), true));
    testResults.add(new TestResult(new Pid(0x0002), true));
    testResults.add(new TestResult(new Pid(0x010C), true));
    testResults.add(new TestResult(new Pid(0x0104), false));

    TestReport testReport = new TestReport(testResults);

    assertThat(testReport.getTests(), containsInAnyOrder(testResults.toArray()));
    assertThat(testReport.supports(new Pid("00 01")), equalTo(true));
    assertThat(testReport.supports(new Pid("0002")), equalTo(true));
    assertThat(testReport.supports(new Pid("0003")), equalTo(false));
    assertThat(testReport.supports(new Pid("010C")), equalTo(true));
    assertThat(testReport.supports(new Pid(0x0104)), equalTo(false));
  }
}
