package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class VinParserTests {
  @Test
  public void testCanResponse() throws Exception {
    String obd = "014\r"
               + "0: 49 02 01 31 44 34\r"
               + "1: 47 50 30 30 52 35 35\r"
               + "2: 42 31 32 33 34 35 36\r";

    VinParser parser = new VinParser();
    String response = parser.parse(obd);

    assertThat(response, equalTo("1D4GP00R55B123456"));
  }

  @Test(expected = UnexpectedResponseException.class)
  public void testCanResponseWithInvalidHexDigits() throws Exception {
    String obd = "014\r"
               + "0: 49 02 01 31 44 34\r"
               + "1: 47 50 30 30 52 35 35\r"
               + "2: 42 31 32 33 34 35 $$\r";

    VinParser parser = new VinParser();
    parser.parse(obd);
  }

  @Test(expected = UnexpectedResponseException.class)
  public void testCanResponseWithUnexpectedByteCount() throws Exception {
    String obd = "013\r"
               + "0: 49 02 01 31 44 34\r"
               + "1: 47 50 30 30 52 35 35\r"
               + "2: 42 31 32 33 34 35 36\r";

    VinParser parser = new VinParser();
    parser.parse(obd);
  }

  @Test(expected = UnexpectedResponseException.class)
  public void testCanResponseWithTooManyLines() throws Exception {
    String obd = "014\r"
               + "0: 49 02 01 31 44 34\r"
               + "1: 47 50 30 30 52 35 35\r"
               + "2: 42 31 32 33 34 35 36\r"
               + "3: 42 31 32 33 34 35 36\r";

    VinParser parser = new VinParser();
    parser.parse(obd);
  }
}
