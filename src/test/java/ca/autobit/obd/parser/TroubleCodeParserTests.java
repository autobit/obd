package ca.autobit.obd.parser;

import ca.autobit.obd.TroubleCode;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;

public class TroubleCodeParserTests {
  @Test
  public void parseMultiLineCanResponse() throws Exception {
    String obd = "00A\r"
               + "0: 43 04 01 43 01 96\r"
               + "1: 01 34 01 05 00 00 00\r";

    TroubleCodeParser parser = new TroubleCodeParser();
    Set<TroubleCode> troubleCodes = parser.parse(obd);
    List<String> troubleCodeValues = new ArrayList<>();

    for(TroubleCode dtc : troubleCodes) {
      troubleCodeValues.add(dtc.toString());
    }

    assertThat(troubleCodes.size(), equalTo(4));
    assertThat(troubleCodeValues, containsInAnyOrder("P0143", "P0196", "P0134", "P0105"));
  }

  @Test
  public void testSingleLineCanResponse() throws Exception {
    String obd = "43 01 33 00 00 00 00\r";
    TroubleCodeParser parser = new TroubleCodeParser();
    Set<TroubleCode> troubleCodes = parser.parse(obd);
    List<String> troubleCodeValues = new ArrayList<>();

    for(TroubleCode dtc : troubleCodes) {
      troubleCodeValues.add(dtc.toString());
    }

    assertThat(troubleCodes.size(), equalTo(1));
    assertThat(troubleCodeValues, containsInAnyOrder("P0133"));
  }

  @Test
  public void testInvalidResponse() throws Exception {
    TroubleCodeParser parser = new TroubleCodeParser();
    Set<TroubleCode> troubleCodes = parser.parse("\r");

    assertThat(troubleCodes.size(), equalTo(0));
  }
}
