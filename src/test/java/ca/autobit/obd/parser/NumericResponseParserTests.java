package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class NumericResponseParserTests {
  // A test class we can use to extract the data from.
  private class PassThroughTestParser extends NumericResponseParser<int[]> {
    @Override
    public int[] parse(NumericResponse response) {
      return new int[] {
        response.getA(),
        response.getB(),
        response.getC(),
        response.getD()
      };
    }
  }

  @Test
  public void noDataReturned() throws Exception {
    PassThroughTestParser parser = new PassThroughTestParser();
    int[] response = parser.parse("NO DATA\r");

    assertThat(response, equalTo(null));
  }

  @Test
  public void responseContainsSearching() throws Exception {
    PassThroughTestParser parser = new PassThroughTestParser();
    int[] response = parser.parse("SEARCHING...\r4100 AA BB CC DD\r");

    assertThat(response[0], equalTo(0xAA));
    assertThat(response[1], equalTo(0xBB));
    assertThat(response[2], equalTo(0xCC));
    assertThat(response[3], equalTo(0xDD));
  }

  @Test(expected = UnexpectedResponseException.class)
  public void responseTooShort() throws Exception {
    PassThroughTestParser request = new PassThroughTestParser();
    request.parse("");
  }

  @Test
  public void responseHasInvalidHexDigits() throws Exception {
    PassThroughTestParser parser = new PassThroughTestParser();
    int[] response = parser.parse("4100 EE FF GG HH");

    assertThat(response[0], equalTo(0xEE));
    assertThat(response[1], equalTo(0xFF));
    assertThat(response[2], equalTo(0));
    assertThat(response[3], equalTo(0));
  }
}
