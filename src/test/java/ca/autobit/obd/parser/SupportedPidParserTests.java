package ca.autobit.obd.parser;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class SupportedPidParserTests {
  @Test
  public void allPidsSupported() throws Exception {
    SupportedPidParser parser = new SupportedPidParser();
    Boolean[] result = parser.parse("4100 FF FF FF FF");

    for(Boolean supported : result) {
      assertThat(supported, equalTo(true));
    }

    assertThat(result.length, equalTo(32));
  }

  @Test
  public void somePidsNotSupported() throws Exception {
    SupportedPidParser parser = new SupportedPidParser();
    Boolean[] result = parser.parse("4100 AA AA AA AA");

    for(int i = 0; i < result.length; i++) {
      if(i % 2 == 0) {
        // All even PIDs should be set.
        assertThat(result[i], equalTo(true));
      }
      else {
        // All odd PIDs should not be set.
        assertThat(result[i], equalTo(false));
      }
    }

    assertThat(result.length, equalTo(32));
  }

  @Test
  public void noPidsSupported() throws Exception {
    SupportedPidParser parser = new SupportedPidParser();
    Boolean[] result = parser.parse("4100 00 00 00 00");

    for(Boolean supported : result) {
      assertThat(supported, equalTo(false));
    }

    assertThat(result.length, equalTo(32));
  }
}
