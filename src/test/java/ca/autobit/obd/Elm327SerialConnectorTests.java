package ca.autobit.obd;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.OngoingStubbing;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

public class Elm327SerialConnectorTests {
  private SerialPort mockSerialPort;
  private Elm327SerialConnector connector;

  @Before
  public void setUp() {
    mockSerialPort = mock(SerialPort.class);
    connector = new Elm327SerialConnector(mockSerialPort, 1, TimeUnit.SECONDS);
  }

  @Test
  public void testConnect() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())), "ELM327\r>");

    assertThat(connector.isConnected(), equalTo(false));
    connector.connect();
    assertThat(connector.isConnected(), equalTo(true));
  }

  @Test(expected = CannotConnectException.class)
  public void testConnectIfPortNotOpenable() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(false);

    assertThat(connector.isConnected(), equalTo(false));
    connector.connect();
  }

  @Test(expected = CannotConnectException.class)
  public void testConnectIfNoResponse() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    when(mockSerialPort.readBytes(anyInt(), anyInt())).thenReturn(new byte[] { (byte) '>' });

    assertThat(connector.isConnected(), equalTo(false));

    try {
      connector.connect();
    }
    catch(CannotConnectException e) {
      assertThat(e.getMessage(), containsString("Timeout"));
      throw e;
    }
  }

  @Test(expected = CannotConnectException.class)
  public void testConnectIfUnableToConnectResponse() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>",
      ">",
      ">",
      ">",
      "UNABLE TO CONNECT\r>"
    );

    assertThat(connector.isConnected(), equalTo(false));

    try {
      connector.connect();
    }
    catch(CannotConnectException e) {
      assertThat(e.getMessage(), containsString("Unable to connect"));
      throw e;
    }
  }

  @Test(expected = CannotConnectException.class)
  public void testConnectThenSerialPortException() throws Exception {
    Throwable err = new SerialPortException("", "", "");

    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())), "ELM327\r>").thenThrow(err);

    assertThat(connector.isConnected(), equalTo(false));

    try {
      connector.connect();
    }
    catch(CannotConnectException e) {
      assertThat(e.getCause(), equalTo(err));
      throw e;
    }
  }

  @Test(expected = CannotConnectException.class)
  public void testConnectThenSerialPortTimeoutException() throws Exception {
    Throwable err = new SerialPortTimeoutException("", "", 0);

    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())), "ELM327\r>").thenThrow(err);

    assertThat(connector.isConnected(), equalTo(false));

    try {
      connector.connect();
    }
    catch(CannotConnectException e) {
      assertThat(e.getCause(), equalTo(err));
      throw e;
    }
  }

  @Test(expected = IOException.class)
  public void testRequestIfNotConnected() throws Exception {
    connector.request(new Pid("0101"));
  }

  @Test
  public void testRequest() throws Exception {
    String pid = "0100";
    String expected = "4100 FF FF FF FF\r";

    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(
      when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>", ">", ">", ">", ">", expected + ">"
    );

    connector.connect();
    String actual = connector.request(new Pid(pid));

    // Capture the bytes written to the serial port to be sure our OBD command was written.
    ArgumentCaptor<byte[]> writtenToPort = ArgumentCaptor.forClass(byte[].class);
    verify(mockSerialPort, atLeastOnce()).writeBytes(writtenToPort.capture());
    List<byte[]> bytesWritten = writtenToPort.getAllValues();

    assertThat(actual, equalTo(expected));
    assertThat(bytesWritten, hasItem((pid + "\r\n").getBytes()));
  }

  @Test
  public void testRequestWithCanError() throws Exception {
    String pid = "0100";
    String expected = "4100 FF FF FF FF\r";

    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(
      when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>", ">", ">", ">", ">", "CAN ERROR\r>", expected + ">"
    );

    connector.connect();
    String actual = connector.request(new Pid(pid));

    // Capture the bytes written to the serial port to be sure our OBD command was written.
    ArgumentCaptor<byte[]> writtenToPort = ArgumentCaptor.forClass(byte[].class);
    verify(mockSerialPort, atLeastOnce()).writeBytes(writtenToPort.capture());
    List<byte[]> bytesWritten = writtenToPort.getAllValues();

    assertThat(actual, equalTo(expected));
    assertThat(bytesWritten, hasItem((pid + "\r\n").getBytes()));
  }

  @Test(expected = CannotConnectException.class)
  public void testRequestWhenUnableToConnect() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(
      when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>", ">", ">", ">", ">", "UNABLE TO CONNECT\r>"
    );

    connector.connect();
    connector.request(new Pid("0100"));
  }

  @Test
  public void testRequestWithNoData() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(
      when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>", ">", ">", ">", ">", "NO DATA\r>"
    );

    connector.connect();
    String response = connector.request(new Pid("0100"));

    assertThat(response, equalTo(null));
  }

  @Test(expected = IOException.class)
  public void testRequestThrowingSerialPortException() throws Exception {
    Throwable err = new SerialPortException("", "", "");

    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(
      when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>", ">", ">", ">", ">"
    ).thenThrow(err);

    connector.connect();

    try {
      connector.request(new Pid("0100"));
    }
    catch(IOException e) {
      assertThat(e.getCause(), equalTo(err));
      throw e;
    }
  }

  @Test(expected = IOException.class)
  public void testRequestThrowingSerialPortTimeoutException() throws Exception {
    Throwable err = new SerialPortTimeoutException("", "", 0);

    when(mockSerialPort.openPort()).thenReturn(true);
    replyAsBytes(
      when(mockSerialPort.readBytes(anyInt(), anyInt())),
      "ELM327\r>", ">", ">", ">", ">"
    ).thenThrow(err);

    connector.connect();

    try {
      connector.request(new Pid("0100"));
    }
    catch(IOException e) {
      assertThat(e.getCause(), equalTo(err));
      throw e;
    }
  }

  @Test
  public void testCloseConnection() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    when(mockSerialPort.isOpened()).thenReturn(true);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())), "ELM327\r>");

    assertThat(connector.isConnected(), equalTo(false));
    connector.connect();
    assertThat(connector.isConnected(), equalTo(true));
    connector.close();
    assertThat(connector.isConnected(), equalTo(false));
  }

  @Test
  public void testCloseConnectionWhenPortNotOpen() throws Exception {
    when(mockSerialPort.openPort()).thenReturn(true);
    when(mockSerialPort.isOpened()).thenReturn(false);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())), "ELM327\r>");

    assertThat(connector.isConnected(), equalTo(false));
    connector.connect();
    assertThat(connector.isConnected(), equalTo(true));
    connector.close();
    assertThat(connector.isConnected(), equalTo(false));
  }

  @Test(expected = IOException.class)
  public void testCloseConnectionWithSerialPortException() throws Exception {
    Throwable err = new SerialPortException("", "", "");

    when(mockSerialPort.openPort()).thenReturn(true);
    when(mockSerialPort.isOpened()).thenReturn(true);
    when(mockSerialPort.closePort()).thenThrow(err);
    replyAsBytes(when(mockSerialPort.readBytes(anyInt(), anyInt())), "ELM327\r>");

    assertThat(connector.isConnected(), equalTo(false));
    connector.connect();
    assertThat(connector.isConnected(), equalTo(true));

    try {
      connector.close();
    }
    catch(IOException e) {
      assertThat(e.getCause(), equalTo(err));
      throw e;
    }
  }

  private static OngoingStubbing<byte[]> replyAsBytes(OngoingStubbing<byte[]> stub, String... strings) {
    StringBuilder builder = new StringBuilder();

    for(String string : strings) {
      builder.append(string);
    }

    String s = builder.toString();

    if(s.length() < 1) {
      return stub.thenReturn(new byte[0]);
    }
    else if(s.length() == 1) {
      return stub.thenReturn(new byte[] { (byte) s.charAt(0) });
    }
    else {
      byte[][] rest = new byte[s.length() - 1][1];

      for(int i = 1; i < s.length(); i++) {
        rest[i - 1][0] = (byte) s.charAt(i);
      }

      return stub.thenReturn(new byte[] { (byte) s.charAt(0) }, rest);
    }
  }
}
