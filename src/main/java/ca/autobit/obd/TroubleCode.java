package ca.autobit.obd;

import java.io.Serializable;
import java.util.Date;

public class TroubleCode implements Serializable {
  private char type;
  private int code;
  private boolean pending;
  private boolean permanent;
  private Date generatedDate;

  public void setType(char type) {
    this.type = type;
  }

  public char getType() {
    return type;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public int getCode() {
    return code;
  }

  public boolean isPending() {
    return pending;
  }

  public void setPending(boolean pending) {
    this.pending = pending;
  }

  public boolean isPermanent() {
    return permanent;
  }

  public void setPermanent(boolean permanent) {
    this.permanent = permanent;
  }

  public Date getGeneratedDate() {
    return generatedDate;
  }

  public void setGeneratedDate(Date generatedDate) {
    this.generatedDate = generatedDate;
  }

  @Override
  public String toString() {
    return String.format("%c%04x", type, code);
  }

  public static TroubleCode from(String code) {
    code = code.toUpperCase();
    char type = code.charAt(0);
    int hex = Integer.parseInt(code.substring(1), 16);
    TroubleCode troubleCode = new TroubleCode();

    troubleCode.setType(type);
    troubleCode.setCode(hex);

    return troubleCode;
  }
}