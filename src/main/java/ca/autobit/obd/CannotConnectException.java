package ca.autobit.obd;

import java.io.IOException;

public class CannotConnectException extends IOException {
  public CannotConnectException() {
    super();
  }

  public CannotConnectException(Throwable e) {
    super(e);
  }

  public CannotConnectException(String message) {
    super(message);
  }
}
