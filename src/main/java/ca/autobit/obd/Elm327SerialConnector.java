package ca.autobit.obd;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortTimeoutException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Elm327SerialConnector implements Connector {
  public static final long MAX_RESPONSE_BYTE_COUNT = 512_000;

  private final SerialPort serialPort;
  private final int timeout;
  private final TimeUnit timeoutUnit;
  private boolean pendingRequest;
  private boolean connected;

  public Elm327SerialConnector(String portName) {
    this(portName, 10, TimeUnit.SECONDS);
  }

  public Elm327SerialConnector(String portName, int timeout, TimeUnit timeoutUnit) {
    this(new SerialPort(portName), timeout, timeoutUnit);
  }

  public Elm327SerialConnector(SerialPort serialPort, int timeout, TimeUnit timeoutUnit) {
    this.serialPort = serialPort;
    this.timeout = timeout;
    this.timeoutUnit = timeoutUnit;
    this.pendingRequest = false;
    this.connected = false;
  }

  @Override
  public void connect() throws CannotConnectException {
    synchronized(this) {
      if(connected) {
        // Already connected!
        return;
      }

      try {
        if(!serialPort.openPort()) {
          throw new CannotConnectException("Unable to open the serial port");
        }

        serialPort.setParams(SerialPort.BAUDRATE_38400, 8, 1, 0);
        sendRequest("AT Z");

        long startTime = System.currentTimeMillis();
        long maxWaitTime = timeoutUnit.toMillis(timeout);
        long currentTime;
        String response = readResponse();

        while(response == null || !response.contains("ELM327")) {
          currentTime = System.currentTimeMillis();

          if((currentTime - startTime) > maxWaitTime) {
            throw new CannotConnectException("Timeout while waiting for ELM327 to reply.");
          }

          response = readResponse();
        }

        sendRequest("AT E0");
        sendRequest("AT SP 0");
        sendRequest("AT H0"); // TODO Turn on headers. Parsers will need to be updated.
        sendRequest("0100");
        response = readResponse();

        if(response.contains("UNABLE TO CONNECT")) {
          throw new CannotConnectException("Unable to connect to the vehicle");
        }

        connected = true;
      }
      catch(SerialPortException | SerialPortTimeoutException e) {
        throw new CannotConnectException(e);
      }
    }
  }

  @Override
  public void close() throws IOException {
    synchronized(this) {
      if(!connected) {
        // Already disconnected!
        return;
      }

      try {
        if(serialPort.isOpened()) {
          serialPort.closePort();
        }

        connected = false;
      }
      catch(SerialPortException e) {
        throw new IOException(e);
      }
    }
  }

  @Override
  public boolean isConnected() {
    synchronized(this) {
      return connected;
    }
  }

  @Override
  public String request(Pid pid) throws IOException {
    synchronized(this) {
      if(!connected) {
        throw new IOException("Not connected to ELM327");
      }

      try {
        sendRequest(pid.getPid());
        String response = readResponse();

        if(response == null) {
          return null;
        }
        else if(response.contains("NO DATA")) {
          return null;
        }
        else if(response.contains("CAN ERROR")) {
          return request(pid);
        }
        else if(response.contains("UNABLE TO CONNECT")) {
          throw new CannotConnectException();
        }

        return response;
      }
      catch(SerialPortException | SerialPortTimeoutException e) {
        throw new IOException(e);
      }
    }
  }

  private void sendRequest(String request) throws SerialPortException, SerialPortTimeoutException {
    if(pendingRequest) {
      flushResponse();
    }

    pendingRequest = true;
    request += "\r\n";
    serialPort.writeBytes(request.getBytes());
  }

  private String readResponse() throws SerialPortException, SerialPortTimeoutException {
    if(!pendingRequest) {
      return null;
    }

    byte c;
    byte end = (byte) '>';
    int timeoutMillis = (int) timeoutUnit.toMillis(timeout);
    List<Byte> boxedResult = new ArrayList<>();

    while((c = serialPort.readBytes(1, timeoutMillis)[0]) != end && boxedResult.size() <= MAX_RESPONSE_BYTE_COUNT) {
      boxedResult.add(c);
    }

    byte[] result = new byte[boxedResult.size()];
    for(int i = 0; i < result.length; i++) {
      result[i] = boxedResult.get(i);
    }

    pendingRequest = false;
    return new String(result);
  }

  private void flushResponse() throws SerialPortException, SerialPortTimeoutException {
    readResponse();
    pendingRequest = false;
  }
}

