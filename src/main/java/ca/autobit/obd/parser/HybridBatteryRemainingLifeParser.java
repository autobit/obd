package ca.autobit.obd.parser;

public class HybridBatteryRemainingLifeParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA() * 100 / 255;
  }
}
