package ca.autobit.obd.parser;

public class FuelInjectionRateParser extends NumericResponseParser<Double> {
  @Override
  public Double parse(NumericResponse response) {
    return ((response.getA() * 256) + response.getB()) * 0.05;
  }
}
