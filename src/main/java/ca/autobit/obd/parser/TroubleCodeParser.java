package ca.autobit.obd.parser;

import ca.autobit.obd.TroubleCode;
import ca.autobit.obd.UnexpectedResponseException;
import ca.autobit.obd.util.StringUtil;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class TroubleCodeParser implements ResponseParser<Set<TroubleCode>> {
  @Override
  public Set<TroubleCode> parse(String response) throws UnexpectedResponseException {
    response = response.trim();
    Set<TroubleCode> troubleCodeList = new HashSet<>();
    String troubleCodeString;
    int ignoreBytes;
    StringUtil stringUtil = StringUtil.getInstance();

    /*
     * Response: (multiline)
     * 00A (\n)
     * 0: 43 04 01 43 01 96 (\n)
     * 1: 01 34 01 05 00 00 00 (\n)
     *
     * OR: (single line)
     * 43 01 33 00 00 00 00
     */

    if(stringUtil.getLineCount(response) > 1) {
      // Multiline response, need to convert into single line.
      // Line 1 = number of bytes that are not padding.
      // Line 2 = first frame
      // Line n = n+1 frame
      String[] lines = response.split(StringUtil.NEW_LINE_PATTERN);
      StringBuilder troubleCodeStringBuilder = new StringBuilder();

      for(String line : lines) {
        if(!line.contains(":")) {
          continue;
        }

        line = line.split(":")[1]; // Remove the line number from the line.
        troubleCodeStringBuilder.append(stringUtil.stripNonAlphaNumeric(line, true));
      }

      troubleCodeString = troubleCodeStringBuilder.toString();
      ignoreBytes = 2;
    }
    else {
      troubleCodeString = stringUtil.stripNonAlphaNumeric(response, true);
      ignoreBytes = 1;
    }

    if(troubleCodeString.length() < ignoreBytes * 2) {
      return troubleCodeList;
    }

    // Always ignore the first 2 byte in the response.
    troubleCodeString = troubleCodeString.substring(ignoreBytes * 2);

    // Read each trouble code. Parse substrings 4 characters at a time. Ex: 4143 -> C0143
    for(int i = 0; i < troubleCodeString.length(); i += 4) {
      String hexCode;

      try {
        hexCode = troubleCodeString.substring(i, i + 4);
      }
      catch(StringIndexOutOfBoundsException e) {
        break;
      }

      int firstByte = Integer.parseInt(Character.toString(hexCode.charAt(0)), 16);
      String restBytes = hexCode.substring(1);
      char type = dtcTypeFromInt(firstByte >> 2);
      int mostSignificantDigit = firstByte % 4;
      int code = Integer.parseInt(mostSignificantDigit + restBytes, 16);

      if(code != 0) {
        TroubleCode troubleCode = new TroubleCode();

        troubleCode.setGeneratedDate(new Date());
        troubleCode.setCode(code);
        troubleCode.setType(type);
        troubleCodeList.add(troubleCode);
      }
    }

    return troubleCodeList;
  }

  private char dtcTypeFromInt(int digit) {
    char type;

    if(digit < 0 || digit > 3) {
      throw new IllegalArgumentException("DTC type must be in range [0, 3]");
    }

    switch(digit) {
      case 0:
        type = 'P';
        break;
      case 1:
        type = 'C';
        break;
      case 2:
        type = 'B';
        break;
      case 3:
      default:
        type = 'U';
    }

    return type;
  }
}
