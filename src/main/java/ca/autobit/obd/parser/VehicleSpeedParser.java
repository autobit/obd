package ca.autobit.obd.parser;

public class VehicleSpeedParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA();
  }
}
