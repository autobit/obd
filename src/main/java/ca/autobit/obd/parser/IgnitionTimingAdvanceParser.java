package ca.autobit.obd.parser;

public class IgnitionTimingAdvanceParser extends NumericResponseParser<Double> {
  @Override
  public Double parse(NumericResponse response) {
    return (response.getA() - 128) / 2.0;
  }
}
