package ca.autobit.obd.parser;

import ca.autobit.obd.TroubleCode;
import ca.autobit.obd.UnexpectedResponseException;

import java.util.Set;

public class PendingTroubleCodeParser extends TroubleCodeParser {
  @Override
  public Set<TroubleCode> parse(String response) throws UnexpectedResponseException {
    Set<TroubleCode> troubleCodes = super.parse(response);

    for(TroubleCode troubleCode : troubleCodes) {
      troubleCode.setPending(true);
    }

    return troubleCodes;
  }
}
