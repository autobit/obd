package ca.autobit.obd.parser;

public class FuelInjectionTimingParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return (((response.getA() * 256) + response.getB()) - 26880) / 128;
  }
}
