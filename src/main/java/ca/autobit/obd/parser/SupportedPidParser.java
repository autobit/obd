package ca.autobit.obd.parser;

import java.util.Arrays;

public final class SupportedPidParser extends NumericResponseParser<Boolean[]> {
  @Override
  public Boolean[] parse(NumericResponse response) {
    Boolean[] supported = new Boolean[0x20];
    Arrays.fill(supported, false);

    // Turn A = FF, B = FF, C = FF, D = FF into flags = FFFFFFFF for example.
    int flags = response.getResponse();

    for(int i = 0; i < 0x20; flags = flags << 1, i++) {
      supported[i] = ((flags >> 31) & 1) == 1;
    }

    return supported;
  }
}
