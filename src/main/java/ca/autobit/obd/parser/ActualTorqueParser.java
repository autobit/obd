package ca.autobit.obd.parser;

public class ActualTorqueParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA() - 125;
  }
}
