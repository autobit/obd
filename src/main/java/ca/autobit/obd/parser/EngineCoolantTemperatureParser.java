package ca.autobit.obd.parser;

public class EngineCoolantTemperatureParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA() - 40;
  }
}
