package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;

public final class GenericNumericResponseParser implements ResponseParser<NumericResponse> {
  private static final int MIN_RESPONSE_LENGTH_EXPECTED = 6;

  @Override
  public NumericResponse parse(String response) throws UnexpectedResponseException {
    // Handle different types of responses.
    if(response == null || response.contains("NO DATA")) {
      return null;
    }
    else if(response.contains("SEARCHING")) {
      String[] responseParts = response.split("[\\r\\n]+");
      response = responseParts[responseParts.length - 1];
    }

    response = response.replaceAll("[^a-fA-F0-9]", "");

    if(response.length() < MIN_RESPONSE_LENGTH_EXPECTED) {
      throw new UnexpectedResponseException();
    }

    int[] buffer = { 0, 0, 0, 0 };

    // Get last 4 bytes.
    response = response.substring(4, response.length());

    for(int i = 0; i < 4; i++) {
      String hex;

      try {
        int startIndex = i * 2;
        int endIndex = startIndex + 2;
        hex = response.substring(startIndex, endIndex);
        buffer[i] = Integer.parseInt(hex, 16);
      }
      catch(IndexOutOfBoundsException e) {
        break;
      }
      catch(NumberFormatException e) {
        buffer[i] = 0;
      }
    }

    return new NumericResponse(buffer[0], buffer[1], buffer[2], buffer[3]);
  }
}
