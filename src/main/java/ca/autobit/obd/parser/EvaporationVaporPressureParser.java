package ca.autobit.obd.parser;

public class EvaporationVaporPressureParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return ((response.getA() * 256) + response.getB()) - 32767;
  }
}
