package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;

public interface ResponseParser<T> {
  T parse(String response) throws UnexpectedResponseException;
}
