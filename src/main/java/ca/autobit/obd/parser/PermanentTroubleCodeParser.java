package ca.autobit.obd.parser;

import ca.autobit.obd.TroubleCode;
import ca.autobit.obd.UnexpectedResponseException;

import java.util.Set;

public class PermanentTroubleCodeParser extends TroubleCodeParser {
  @Override
  public Set<TroubleCode> parse(String rawResponse) throws UnexpectedResponseException {
    Set<TroubleCode> troubleCodes = super.parse(rawResponse);

    for(TroubleCode troubleCode : troubleCodes) {
      troubleCode.setPermanent(true);
    }

    return troubleCodes;
  }
}
