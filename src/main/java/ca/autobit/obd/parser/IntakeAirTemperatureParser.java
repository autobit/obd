package ca.autobit.obd.parser;

public class IntakeAirTemperatureParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA() - 40;
  }
}
