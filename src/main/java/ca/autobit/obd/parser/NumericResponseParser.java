package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;

public abstract class NumericResponseParser<T> implements ResponseParser<T> {
  private static final GenericNumericResponseParser numericResponseParser = new GenericNumericResponseParser();

  protected abstract T parse(NumericResponse response);

  @Override
  public T parse(String response) throws UnexpectedResponseException {
    NumericResponse numericResponse = numericResponseParser.parse(response);

    if(numericResponse == null) {
      return null;
    }

    return parse(numericResponse);
  }
}
