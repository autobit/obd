package ca.autobit.obd.parser;

public class MonitorStatusParser extends NumericResponseParser<MonitorStatus> {
  @Override
  public MonitorStatus parse(NumericResponse response) {
    boolean engineLightOn = (response.getA() & 0x80) == 0x80; // Get bit A[7]
    int dtcCount = response.getA() & 0x7F; // Get bits A[6..0]

    return new MonitorStatus(dtcCount, engineLightOn);
  }
}
