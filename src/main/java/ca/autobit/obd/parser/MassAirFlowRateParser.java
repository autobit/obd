package ca.autobit.obd.parser;

public class MassAirFlowRateParser extends NumericResponseParser<Double> {
  @Override
  public Double parse(NumericResponse response) {
    return ((response.getA() * 256) + response.getB()) / 100.0;
  }
}
