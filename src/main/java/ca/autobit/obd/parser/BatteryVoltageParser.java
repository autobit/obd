package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;

public class BatteryVoltageParser implements ResponseParser<Double> {
  @Override
  public Double parse(String response) throws UnexpectedResponseException {
    return Double.parseDouble(response.replace("V", ""));
  }
}
