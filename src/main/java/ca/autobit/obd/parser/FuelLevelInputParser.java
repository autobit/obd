package ca.autobit.obd.parser;

public class FuelLevelInputParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA() * 100 / 255;
  }
}
