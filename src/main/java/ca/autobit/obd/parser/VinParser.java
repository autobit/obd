package ca.autobit.obd.parser;

import ca.autobit.obd.UnexpectedResponseException;
import ca.autobit.obd.util.StringUtil;

public class VinParser implements ResponseParser<String> {
  @Override
  public String parse(String response) throws UnexpectedResponseException {
    StringUtil stringUtil = StringUtil.getInstance();

    if(response.contains("NO DATA") || response.contains("SEARCHING")) {
      return null;
    }

    String[] lines = response.split("[\\r\\n]+");
    char[] characters = new char[17];
    int b = 0;

    if(lines.length != 4 || !lines[0].contains("014")) {
      // Vehicle responds first with the hex number of bytes in the response. 0x014 = 20 bytes
      throw new UnexpectedResponseException();
    }

    for(int i = 1; i < lines.length && b < characters.length; i++) {
      String line = stringUtil.stripNonAlphaNumeric(lines[i], true);
      int start = (i == 1) ? 7 : 1; // Ignore first 7 characters on the first loop.

      for(int j = start; j < line.length() && b < characters.length; j = j + 2) {
        String hex = line.substring(j, j + 2);

        try {
          characters[b] = (char) Integer.parseInt(hex, 16);
        }
        catch(NumberFormatException e) {
          characters[b] = '?';
        }

        b++;
      }
    }

    if(b != 17) {
      throw new UnexpectedResponseException("Expected a VIN with 17 characters, got " + b);
    }

    return new String(characters);
  }
}
