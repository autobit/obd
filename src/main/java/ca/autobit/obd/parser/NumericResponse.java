package ca.autobit.obd.parser;

public final class NumericResponse {
  private final int a;
  private final int b;
  private final int c;
  private final int d;

  public NumericResponse(int a, int b, int c, int d) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
  }

  public int getA() {
    return a;
  }

  public int getB() {
    return b;
  }

  public int getC() {
    return c;
  }

  public int getD() {
    return d;
  }

  public int getResponse() {
    // Turns A = 11, B = 22, C = EE, D = FF into 1122EEFF for example.
    return (a << 24) | (b << 16) | (c << 8) | d;
  }
}
