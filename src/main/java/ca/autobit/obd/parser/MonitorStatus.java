package ca.autobit.obd.parser;

public class MonitorStatus {
  private final int troubleCodeCount;
  private final boolean engineLightOn;

  public MonitorStatus(int troubleCodeCount, boolean engineLightOn) {
    this.troubleCodeCount = troubleCodeCount;
    this.engineLightOn = engineLightOn;
  }

  public int getTroubleCodeCount() {
    return troubleCodeCount;
  }

  public boolean isEngineLightOn() {
    return engineLightOn;
  }
}
