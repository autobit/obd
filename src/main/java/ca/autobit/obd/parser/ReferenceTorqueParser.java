package ca.autobit.obd.parser;

public class ReferenceTorqueParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return (response.getA() * 256) + response.getB();
  }
}
