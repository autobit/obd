package ca.autobit.obd.parser;

public class FuelPressureParser extends NumericResponseParser<Integer> {
  @Override
  public Integer parse(NumericResponse response) {
    return response.getA() * 3;
  }
}
