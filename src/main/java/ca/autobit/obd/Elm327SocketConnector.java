package ca.autobit.obd;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Elm327SocketConnector implements Connector {
  public static final long MAX_RESPONSE_BYTE_COUNT = 512_000;

  private final Socket socket;
  private final SocketAddress address;
  private final int timeout;
  private final TimeUnit timeUnit;
  private boolean pendingRequest;
  private boolean connected;

  public Elm327SocketConnector(InetAddress address, int port, int timeout, TimeUnit timeUnit) {
    this.socket = new Socket();
    this.address = new InetSocketAddress(address, port);
    this.timeout = timeout;
    this.timeUnit = timeUnit;
    this.pendingRequest = false;
    this.connected = false;
  }

  @Override
  public void connect() throws CannotConnectException {
    synchronized(this) {
      if(connected) {
        // Already connected.
        return;
      }

      try {
        socket.connect(address, (int) timeUnit.toMillis(timeout));

        sendRequest("AT Z");

        long startTime = System.currentTimeMillis();
        long maxWaitTime = timeUnit.toMillis(timeout);
        long currentTime;
        String response = readResponse();

        while(response == null || !response.contains("ELM327")) {
          currentTime = System.currentTimeMillis();

          if((currentTime - startTime) > maxWaitTime) {
            throw new CannotConnectException("Timeout while waiting for ELM327 to reply.");
          }

          response = readResponse();
        }

        sendRequest("AT E0");
        sendRequest("AT SP 0");
        sendRequest("AT H0"); // TODO Turn on headers. Parsers will need to be updated.
        sendRequest("0100");
        response = readResponse();

        if(response == null || response.contains("UNABLE TO CONNECT")) {
          throw new CannotConnectException("Unable to connect to the vehicle");
        }

        connected = true;
      }
      catch(IOException e) {
        throw new CannotConnectException(e);
      }
    }
  }

  @Override
  public boolean isConnected() {
    synchronized(this) {
      return connected;
    }
  }

  @Override
  public String request(Pid pid) throws IOException {
    synchronized(this) {
      if(!connected) {
        throw new IOException("Not connected to ELM327");
      }

      sendRequest(pid.getPid());
      String response = readResponse();

      if(response == null) {
        return null;
      }
      else if(response.contains("NO DATA")) {
        return null;
      }
      else if(response.contains("CAN ERROR")) {
        return request(pid);
      }
      else if(response.contains("UNABLE TO CONNECT")) {
        throw new CannotConnectException();
      }

      return response;
    }
  }

  @Override
  public void close() throws IOException {
    synchronized(this) {
      if(!connected) {
        // Already disconnected.
        return;
      }

      socket.close();
      connected = false;
    }
  }

  private void sendRequest(String request) throws IOException {
    if(pendingRequest) {
      flushResponse();
    }

    pendingRequest = true;
    request += "\r\n";
    OutputStream outputStream = socket.getOutputStream();

    outputStream.write(request.getBytes());
  }

  private String readResponse() throws IOException {
    if(!pendingRequest) {
      return null;
    }

    byte c;
    byte end = (byte) '>';
    List<Byte> boxedResult = new ArrayList<>();
    InputStream inputStream = socket.getInputStream();

    while((c = (byte) inputStream.read()) != end && c > -1 && boxedResult.size() < MAX_RESPONSE_BYTE_COUNT) {
      boxedResult.add(c);
    }

    byte[] result = new byte[boxedResult.size()];
    for(int i = 0; i < result.length; i++) {
      result[i] = boxedResult.get(i);
    }

    pendingRequest = false;
    return new String(result);
  }

  private void flushResponse() throws IOException {
    readResponse();
    pendingRequest = false;
  }
}
