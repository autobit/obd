package ca.autobit.obd;

import java.io.IOException;

public class UnexpectedResponseException extends IOException {
  public UnexpectedResponseException() {
    super();
  }

  public UnexpectedResponseException(String msg) {
    super(msg);
  }

  public UnexpectedResponseException(Throwable throwable) {
    super(throwable);
  }
}
