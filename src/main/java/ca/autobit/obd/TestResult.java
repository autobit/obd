package ca.autobit.obd;

import java.io.Serializable;

public class TestResult implements Serializable {
  private final Pid pid;
  private final boolean supported;

  public TestResult(Pid pid, boolean supported) {
    this.pid = pid;
    this.supported = supported;
  }

  @Override
  public String toString() {
    return pid.getPid();
  }

  public Pid getPid() {
    return pid;
  }

  public boolean isSupported() {
    return supported;
  }
}
