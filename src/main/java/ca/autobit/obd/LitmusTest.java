package ca.autobit.obd;

import ca.autobit.obd.parser.SupportedPidParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LitmusTest {
  private final Connector connector;
  private final SupportedPidParser responseParser;

  public LitmusTest(Connector connector) {
    this.connector = connector;
    this.responseParser = new SupportedPidParser();
  }

  public TestReport testMode1() throws IOException {
    List<TestResult> tests = new ArrayList<>();

    // Array to hold all results
    Boolean[] mode1Supported = new Boolean[0x100];
    Arrays.fill(mode1Supported, false);

    // Results if a request is unsuccessful
    Boolean[] defaultRange = new Boolean[0x20];
    Arrays.fill(defaultRange, false);

    for(int i = 0; i < 0xFF; i+= 0x20) {
      int code = 0x0100 | i;
      Boolean[] supportedInRange;

      try {
        supportedInRange = responseParser.parse(connector.request(new Pid(code)));

        if(supportedInRange == null) {
          supportedInRange = defaultRange;
        }
      }
      catch(UnexpectedResponseException e) {
        supportedInRange = defaultRange;
      }

      System.arraycopy(supportedInRange, 0, mode1Supported, i, supportedInRange.length);
    }

    for(int i = 0; i < mode1Supported.length - 1; i++) {
      tests.add(new TestResult(new Pid(0x0101 + i), mode1Supported[i]));
    }

    return new TestReport(tests);
  }

  public TestReport testMode9() throws IOException {
    List<TestResult> tests = new ArrayList<>();
    Boolean[] mode9Supported;

    try {
      mode9Supported = responseParser.parse(connector.request(new Pid(0x0900)));

      if(mode9Supported == null) {
        mode9Supported = new Boolean[0x20];
        Arrays.fill(mode9Supported, false);
      }
    }
    catch(UnexpectedResponseException e) {
      mode9Supported = new Boolean[0x20];
      Arrays.fill(mode9Supported, false);
    }

    for(int i = 0; i < mode9Supported.length - 1; i++) {
      tests.add(new TestResult(new Pid(0x0901 + i), mode9Supported[i]));
    }

    return new TestReport(tests);
  }
}