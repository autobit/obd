package ca.autobit.obd;

import java.io.Closeable;
import java.io.IOException;

public interface Connector extends Closeable, AutoCloseable {
  void connect() throws CannotConnectException;

  boolean isConnected();

  String request(Pid pid) throws IOException;
}
