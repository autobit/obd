package ca.autobit.obd;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Pid implements Serializable {
  private static final Map<Pid, String> dictionary = new HashMap<>();

  public static final Pid PIDS_SUPPORTED_01_20 = new Pid(0x0100);
  public static final Pid MONITOR_STATUS_SINCE_DTCS_CLEARED = new Pid(0x0101);
  public static final Pid FREEZE_DTC = new Pid(0x0102);
  public static final Pid FUEL_SYSTEM_STATUS = new Pid(0x0103);
  public static final Pid CALCULATED_ENGINE_LOAD = new Pid(0x0104);
  public static final Pid ENGINE_COOLANT_TEMPERATURE = new Pid(0x0105);
  public static final Pid SHORT_TERM_FUEL_TRIM_BANK_1 = new Pid(0x0106);
  public static final Pid LONG_TERM_FUEL_TRIM_BANK_1 = new Pid(0x0107);
  public static final Pid SHORT_TERM_FUEL_TRIM_BANK_2 = new Pid(0x0108);
  public static final Pid LONG_TERM_FUEL_TRIM_BANK_2 = new Pid(0x0108);
  public static final Pid FUEL_GAUGE_PRESSURE = new Pid(0x010A);
  public static final Pid INTAKE_MANIFOLD_ABSOLUTE_PRESSURE = new Pid(0x010B);
  public static final Pid ENGINE_RPM = new Pid(0x010C);
  public static final Pid VEHICLE_SPEED = new Pid(0x010D);
  public static final Pid TIMING_ADVANCE = new Pid(0x010E);
  public static final Pid INTAKE_AIR_TEMPERATURE = new Pid(0x010F);
  public static final Pid MAF_AIR_FLOW_RATE = new Pid(0x0110);
  public static final Pid THROTTLE_POSITION = new Pid(0x0111);
  public static final Pid COMMANDED_SECONDARY_AIR_STATUS = new Pid(0x0112);
  public static final Pid OXYGEN_SENSORS_PRESENT = new Pid(0x0113);
  public static final Pid OXYGEN_SENSOR_1 = new Pid(0x0114);
  public static final Pid OXYGEN_SENSOR_2 = new Pid(0x0115);
  public static final Pid OXYGEN_SENSOR_3 = new Pid(0x0116);
  public static final Pid OXYGEN_SENSOR_4 = new Pid(0x0117);
  public static final Pid OXYGEN_SENSOR_5 = new Pid(0x0118);
  public static final Pid OXYGEN_SENSOR_6 = new Pid(0x0119);
  public static final Pid OXYGEN_SENSOR_7 = new Pid(0x011A);
  public static final Pid OXYGEN_SENSOR_8 = new Pid(0x011B);
  public static final Pid OBD_STANDARDS = new Pid(0x011C);
  public static final Pid AUXILIARY_INPUT_STATUS = new Pid(0x011E);
  public static final Pid RUN_TIME_SINCE_ENGINE_START = new Pid(0x011F);
  public static final Pid PIDS_SUPPORTED_21_40 = new Pid(0x0120);
  public static final Pid DISTANCE_WITH_MIL_ON = new Pid(0x0121);
  public static final Pid FUEL_RAIL_PRESSURE = new Pid(0x0122);
  public static final Pid FUEL_RAIL_GAUGE_PRESSURE = new Pid(0x0123);
  public static final Pid OXYGEN_SENSOR_1_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x0124);
  public static final Pid OXYGEN_SENSOR_2_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x0125);
  public static final Pid OXYGEN_SENSOR_3_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x0126);
  public static final Pid OXYGEN_SENSOR_4_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x0127);
  public static final Pid OXYGEN_SENSOR_5_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x0128);
  public static final Pid OXYGEN_SENSOR_6_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x0129);
  public static final Pid OXYGEN_SENSOR_7_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x012A);
  public static final Pid OXYGEN_SENSOR_8_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE = new Pid(0x012B);
  public static final Pid COMMANDED_EGR = new Pid(0x012C);
  public static final Pid EGR_ERROR = new Pid(0x012D);
  public static final Pid COMMANDED_EVAPORATIVE_PURGE = new Pid(0x012E);
  public static final Pid FUEL_TANK_LEVEL_INPUT = new Pid(0x012F);
  public static final Pid WARM_UPS_SINCE_CODES_CLEARED = new Pid(0x0130);
  public static final Pid DISTANCE_TRAVELED_SINCE_CODES_CLEARED = new Pid(0x0131);
  public static final Pid EVAPORATION_SYSTEM_VAPOR_PRESSURE = new Pid(0x0132);
  public static final Pid ABSOLUTE_BAROMETRIC_PRESSURE = new Pid(0x0133);
  public static final Pid OXYGEN_SENSOR_1_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x0134);
  public static final Pid OXYGEN_SENSOR_2_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x0135);
  public static final Pid OXYGEN_SENSOR_3_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x0136);
  public static final Pid OXYGEN_SENSOR_4_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x0137);
  public static final Pid OXYGEN_SENSOR_5_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x0138);
  public static final Pid OXYGEN_SENSOR_6_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x0139);
  public static final Pid OXYGEN_SENSOR_7_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x013A);
  public static final Pid OXYGEN_SENSOR_8_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT = new Pid(0x013B);
  public static final Pid CATALYST_TEMPERATURE_BANK_1_SENSOR_1 = new Pid(0x013C);
  public static final Pid CATALYST_TEMPERATURE_BANK_2_SENSOR_1 = new Pid(0x013D);
  public static final Pid CATALYST_TEMPERATURE_BANK_1_SENSOR_2 = new Pid(0x013E);
  public static final Pid CATALYST_TEMPERATURE_BANK_2_SENSOR_2 = new Pid(0x013F);
  public static final Pid PIDS_SUPPORTED_41_60 = new Pid(0x0140);
  public static final Pid MONITOR_STATUS_THIS_DRIVE_CYCLE = new Pid(0x0141);
  public static final Pid CONTROL_MODULE_VOLTAGE = new Pid(0x0142);
  public static final Pid ABSOLUTE_LOAD_VALUE = new Pid(0x0143);
  public static final Pid FUEL_AIR_COMMANDED_EQUIVALENCE_RATIO = new Pid(0x0144);
  public static final Pid RELATIVE_THROTTLE_POSITION = new Pid(0x0145);
  public static final Pid AMBIENT_AIR_TEMPERATURE = new Pid(0x0146);
  public static final Pid ABSOLUTE_THROTTLE_POSITION_B = new Pid(0x0147);
  public static final Pid ABSOLUTE_THROTTLE_POSITION_C = new Pid(0x0148);
  public static final Pid ACCELERATOR_PEDAL_POSITION_D = new Pid(0x0149);
  public static final Pid ACCELERATOR_PEDAL_POSITION_E = new Pid(0x014A);
  public static final Pid ACCELERATOR_PEDAL_POSITION_F = new Pid(0x014B);
  public static final Pid COMMANDED_THROTTLE_ACTUATOR = new Pid(0x014C);
  public static final Pid TIME_RUN_WITH_MIL_ON = new Pid(0x014D);
  public static final Pid TIME_SINCE_TROUBLE_CODES_CLEARED = new Pid(0x014E);
  public static final Pid MAX_VALUE_FOR_FUEL_AIR_EQUIVALENCE_RATIO = new Pid(0x014F);
  public static final Pid FUEL_TYPE = new Pid(0x0151);
  public static final Pid ETHANOL_FUEL_PERCENTAGE = new Pid(0x0152);
  public static final Pid ABSOLUTE_EVAPORATION_SYSTEM_VAPOR_PRESSURE = new Pid(0x0153);
  public static final Pid SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_1_BANK_3 = new Pid(0x0155);
  public static final Pid LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_1_BANK_3 = new Pid(0x0156);
  public static final Pid SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_2_BANK_4 = new Pid(0x0157);
  public static final Pid LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_2_BANK_4 = new Pid(0x0158);
  public static final Pid FUEL_RAIL_ABSOLUTE_PRESSURE = new Pid(0x0159);
  public static final Pid RELATIVE_ACCELERATOR_PEDAL_POSITION = new Pid(0x015A);
  public static final Pid HYBRID_BATTERY_PACK_REMAINING_LIFE = new Pid(0x015B);
  public static final Pid ENGINE_OIL_TEMPERATURE = new Pid(0x015C);
  public static final Pid FUEL_INJECTION_TIMING = new Pid(0x015D);
  public static final Pid ENGINE_FUEL_RATE = new Pid(0x015E);
  public static final Pid EMISSION_REQUIREMENTS_TO_WHICH_VEHICLE_IS_DESIGNED = new Pid(0x015F);
  public static final Pid PIDS_SUPPORTED_61_80 = new Pid(0x0160);
  public static final Pid DEMAND_TORQUE = new Pid(0x0161);
  public static final Pid ACTUAL_TORQUE = new Pid(0x0162);
  public static final Pid ENGINE_REFERENCE_TORQUE = new Pid(0x0163);
  public static final Pid ENGINE_PERCENT_TORQUE_DATA = new Pid(0x0164);
  public static final Pid AUXILIARY_IO_SUPPORTED = new Pid(0x0165);
  public static final Pid MASS_AIR_FLOW_SENSOR = new Pid(0x0166);
  public static final Pid INTAKE_AIR_TEMPERATURE_SENSOR = new Pid(0x0168);
  public static final Pid COMMANDED_EGR_AND_EGR_ERROR = new Pid(0x0169);
  public static final Pid EXHAUST_GAS_RECIRCULATION_TEMPERATURE = new Pid(0x016B);
  public static final Pid FUEL_PRESSURE_CONTROL_SYSTEM = new Pid(0x016D);
  public static final Pid INJECTION_PRESSURE_CONTROL_SYSTEM = new Pid(0x016E);
  public static final Pid TURBOCHARGER_COMPRESSOR_INLET_PRESSURE = new Pid(0x016F);
  public static final Pid BOOST_PRESSURE_CONTROL = new Pid(0x0170);
  public static final Pid VARIABLE_GEOMETRY_TURBO_CONTROL = new Pid(0x0171);
  public static final Pid WASTEGATE_CONTROL = new Pid(0x0172);
  public static final Pid EXHAUST_PRESSURE = new Pid(0x0173);
  public static final Pid TURBOCHARGER_RPM = new Pid(0x0174);
  public static final Pid CHARGE_AIR_COOLER_TEMPERATURE = new Pid(0x0177);
  public static final Pid EXHAUST_GAS_TEMPERATURE_BANK_1 = new Pid(0x0178);
  public static final Pid EXHAUST_GAS_TEMPERATURE_BANK_2 = new Pid(0x0179);
  public static final Pid DIESEL_PARTICULATE_FILTER_TEMPERATURE = new Pid(0x017C);
  public static final Pid NOX_NTE_CONTROL_AREA_STATUS = new Pid(0x017D);
  public static final Pid PM_NTE_CONTROL_AREA_STATUS = new Pid(0x017E);
  public static final Pid ENGINE_RUN_TIME = new Pid(0x017F);
  public static final Pid PIDS_SUPPORTED_81_A0 = new Pid(0x0180);
  public static final Pid NOX_SENSOR = new Pid(0x0183);
  public static final Pid MANIFOLD_SURFACE_TEMPERATURE = new Pid(0x0184);
  public static final Pid NOX_REAGENT_SYSTEM = new Pid(0x0185);
  public static final Pid PARTICULATE_MATTER_SENSOR = new Pid(0x0186);
  public static final Pid PIDS_SUPPORTED_A1_C0 = new Pid(0x01A0);
  public static final Pid PIDS_SUPPORTED_C1_E0 = new Pid(0x01C0);
  public static final Pid STORED_DTCS = new Pid("03");
  public static final Pid CLEAR_DTCS = new Pid("04");
  public static final Pid PENDING_DTCS = new Pid("07");
  public static final Pid PERMANENT_DTCS = new Pid("0A");
  public static final Pid IGNITION_VOLTAGE = new Pid("AT IGN");
  public static final Pid BATTERY_VOLTAGE = new Pid("AT RV");
  public static final Pid CURRENT_PROTOCOL = new Pid("AT DP");
  public static final Pid CURRENT_PROTOCOL_NUM = new Pid("AT DPN");

  static {
    dictionary.put(IGNITION_VOLTAGE, "Vehicle Ignition Voltage (On/Off)");
    dictionary.put(BATTERY_VOLTAGE, "Vehicle Battery Voltage");
    dictionary.put(CURRENT_PROTOCOL, "Current Protocol");
    dictionary.put(CURRENT_PROTOCOL_NUM, "Current Protocol Number");
    dictionary.put(PIDS_SUPPORTED_01_20, "PIDs supported 01 20");
    dictionary.put(MONITOR_STATUS_SINCE_DTCS_CLEARED, "Monitor status since DTCs cleared");
    dictionary.put(FREEZE_DTC, "Freeze DTC");
    dictionary.put(FUEL_SYSTEM_STATUS, "Fuel system status");
    dictionary.put(CALCULATED_ENGINE_LOAD, "Calculated engine load");
    dictionary.put(ENGINE_COOLANT_TEMPERATURE, "Engine coolant temperature");
    dictionary.put(SHORT_TERM_FUEL_TRIM_BANK_1, "Short term fuel trim Bank 1");
    dictionary.put(LONG_TERM_FUEL_TRIM_BANK_1, "Long term fuel trim Bank 1");
    dictionary.put(SHORT_TERM_FUEL_TRIM_BANK_2, "Short term fuel trim Bank 2");
    dictionary.put(LONG_TERM_FUEL_TRIM_BANK_2, "Long term fuel trim Bank 2");
    dictionary.put(FUEL_GAUGE_PRESSURE, "Fuel gauge pressure");
    dictionary.put(INTAKE_MANIFOLD_ABSOLUTE_PRESSURE, "Intake manifold absolute pressure");
    dictionary.put(ENGINE_RPM, "Engine RPM");
    dictionary.put(VEHICLE_SPEED, "Vehicle speed");
    dictionary.put(TIMING_ADVANCE, "Timing advance");
    dictionary.put(INTAKE_AIR_TEMPERATURE, "Intake air temperature");
    dictionary.put(MAF_AIR_FLOW_RATE, "MAF air flow rate");
    dictionary.put(THROTTLE_POSITION, "Throttle position");
    dictionary.put(COMMANDED_SECONDARY_AIR_STATUS, "Commanded secondary air status");
    dictionary.put(OXYGEN_SENSORS_PRESENT, "Oxygen sensors present");
    dictionary.put(OXYGEN_SENSOR_1, "Oxygen Sensor 1");
    dictionary.put(OXYGEN_SENSOR_2, "Oxygen Sensor 2");
    dictionary.put(OXYGEN_SENSOR_3, "Oxygen Sensor 3");
    dictionary.put(OXYGEN_SENSOR_4, "Oxygen Sensor 4");
    dictionary.put(OXYGEN_SENSOR_5, "Oxygen Sensor 5");
    dictionary.put(OXYGEN_SENSOR_6, "Oxygen Sensor 6");
    dictionary.put(OXYGEN_SENSOR_7, "Oxygen Sensor 7");
    dictionary.put(OXYGEN_SENSOR_8, "Oxygen Sensor 8");
    dictionary.put(OBD_STANDARDS, "OBD standards");
    dictionary.put(AUXILIARY_INPUT_STATUS, "Auxiliary input status");
    dictionary.put(RUN_TIME_SINCE_ENGINE_START, "Run time since engine start");
    dictionary.put(PIDS_SUPPORTED_21_40, "PIDs supported 21 40");
    dictionary.put(DISTANCE_WITH_MIL_ON, "Distance with MIL on");
    dictionary.put(FUEL_RAIL_PRESSURE, "Fuel Rail Pressure");
    dictionary.put(FUEL_RAIL_GAUGE_PRESSURE, "Fuel Rail Gauge Pressure");
    dictionary.put(OXYGEN_SENSOR_1_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 1 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_2_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 2 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_3_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 3 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_4_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 4 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_5_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 5 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_6_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 6 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_7_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 7 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(OXYGEN_SENSOR_8_FUEL_AIR_EQUIVALENCE_RATIO_VOLTAGE,
      "Oxygen Sensor 8 Fuel Air Equivalence Ratio Voltage");
    dictionary.put(COMMANDED_EGR, "Commanded EGR");
    dictionary.put(EGR_ERROR, "EGR Error");
    dictionary.put(COMMANDED_EVAPORATIVE_PURGE, "Commanded evaporative purge");
    dictionary.put(FUEL_TANK_LEVEL_INPUT, "Fuel Tank Level Input");
    dictionary.put(WARM_UPS_SINCE_CODES_CLEARED, "Warm ups since codes cleared");
    dictionary.put(DISTANCE_TRAVELED_SINCE_CODES_CLEARED, "Distance traveled since codes cleared");
    dictionary.put(EVAPORATION_SYSTEM_VAPOR_PRESSURE, "Evaporation System Vapor Pressure");
    dictionary.put(ABSOLUTE_BAROMETRIC_PRESSURE, "Absolute Barometric Pressure");
    dictionary.put(OXYGEN_SENSOR_1_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 1 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_2_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 2 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_3_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 3 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_4_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 4 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_5_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 5 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_6_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 6 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_7_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 7 Fuel Air Equivalence Ratio Current");
    dictionary.put(OXYGEN_SENSOR_8_FUEL_AIR_EQUIVALENCE_RATIO_CURRENT,
      "Oxygen Sensor 8 Fuel Air Equivalence Ratio Current");
    dictionary.put(CATALYST_TEMPERATURE_BANK_1_SENSOR_1, "Catalyst Temperature Bank 1 Sensor 1");
    dictionary.put(CATALYST_TEMPERATURE_BANK_2_SENSOR_1, "Catalyst Temperature Bank 2 Sensor 1");
    dictionary.put(CATALYST_TEMPERATURE_BANK_1_SENSOR_2, "Catalyst Temperature Bank 1 Sensor 2");
    dictionary.put(CATALYST_TEMPERATURE_BANK_2_SENSOR_2, "Catalyst Temperature Bank 2 Sensor 2");
    dictionary.put(PIDS_SUPPORTED_41_60, "PIDs supported 41 60");
    dictionary.put(MONITOR_STATUS_THIS_DRIVE_CYCLE, "Monitor status this drive cycle");
    dictionary.put(CONTROL_MODULE_VOLTAGE, "Control module voltage");
    dictionary.put(ABSOLUTE_LOAD_VALUE, "Absolute load value");
    dictionary.put(FUEL_AIR_COMMANDED_EQUIVALENCE_RATIO, "Fuel Air commanded equivalence ratio");
    dictionary.put(RELATIVE_THROTTLE_POSITION, "Relative throttle position");
    dictionary.put(AMBIENT_AIR_TEMPERATURE, "Ambient air temperature");
    dictionary.put(ABSOLUTE_THROTTLE_POSITION_B, "Absolute throttle position B");
    dictionary.put(ABSOLUTE_THROTTLE_POSITION_C, "Absolute throttle position C");
    dictionary.put(ACCELERATOR_PEDAL_POSITION_D, "Accelerator pedal position D");
    dictionary.put(ACCELERATOR_PEDAL_POSITION_E, "Accelerator pedal position E");
    dictionary.put(ACCELERATOR_PEDAL_POSITION_F, "Accelerator pedal position F");
    dictionary.put(COMMANDED_THROTTLE_ACTUATOR, "Commanded throttle actuator");
    dictionary.put(TIME_RUN_WITH_MIL_ON, "Time run with MIL on");
    dictionary.put(TIME_SINCE_TROUBLE_CODES_CLEARED, "Time since trouble codes cleared");
    dictionary.put(MAX_VALUE_FOR_FUEL_AIR_EQUIVALENCE_RATIO, "Max value for Fuel Air equivalence ratio");
    dictionary.put(FUEL_TYPE, "Fuel Type");
    dictionary.put(ETHANOL_FUEL_PERCENTAGE, "Ethanol fuel percentage");
    dictionary.put(ABSOLUTE_EVAPORATION_SYSTEM_VAPOR_PRESSURE, "Absolute Evaporation system Vapor Pressure");
    dictionary.put(SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_1_BANK_3,
      "Short term secondary oxygen sensor trim bank 1 bank 3");
    dictionary.put(LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_1_BANK_3,
      "Long term secondary oxygen sensor trim bank 1 bank 3");
    dictionary.put(SHORT_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_2_BANK_4,
      "Short term secondary oxygen sensor trim bank 2 bank 4");
    dictionary.put(LONG_TERM_SECONDARY_OXYGEN_SENSOR_TRIM_BANK_2_BANK_4,
      "Long term secondary oxygen sensor trim bank 2 bank 4");
    dictionary.put(FUEL_RAIL_ABSOLUTE_PRESSURE, "Fuel rail absolute pressure");
    dictionary.put(RELATIVE_ACCELERATOR_PEDAL_POSITION, "Relative accelerator pedal position");
    dictionary.put(HYBRID_BATTERY_PACK_REMAINING_LIFE, "Hybrid battery pack remaining life");
    dictionary.put(ENGINE_OIL_TEMPERATURE, "Engine oil temperature");
    dictionary.put(FUEL_INJECTION_TIMING, "Fuel injection timing");
    dictionary.put(ENGINE_FUEL_RATE, "Engine fuel rate");
    dictionary.put(EMISSION_REQUIREMENTS_TO_WHICH_VEHICLE_IS_DESIGNED,
      "Emission requirements to which vehicle is designed");
    dictionary.put(PIDS_SUPPORTED_61_80, "PIDs supported 61 80");
    dictionary.put(DEMAND_TORQUE, "Demand torque");
    dictionary.put(ACTUAL_TORQUE, "Actual torque");
    dictionary.put(ENGINE_REFERENCE_TORQUE, "Engine reference torque");
    dictionary.put(ENGINE_PERCENT_TORQUE_DATA, "Engine percent torque data");
    dictionary.put(AUXILIARY_IO_SUPPORTED, "Auxiliary IO supported");
    dictionary.put(MASS_AIR_FLOW_SENSOR, "Mass air flow sensor");
    dictionary.put(INTAKE_AIR_TEMPERATURE_SENSOR, "Intake air temperature sensor");
    dictionary.put(COMMANDED_EGR_AND_EGR_ERROR, "Commanded EGR and EGR Error");
    dictionary.put(EXHAUST_GAS_RECIRCULATION_TEMPERATURE, "Exhaust gas recirculation temperature");
    dictionary.put(FUEL_PRESSURE_CONTROL_SYSTEM, "Fuel pressure control system");
    dictionary.put(INJECTION_PRESSURE_CONTROL_SYSTEM, "Injection pressure control system");
    dictionary.put(TURBOCHARGER_COMPRESSOR_INLET_PRESSURE, "Turbocharger compressor inlet pressure");
    dictionary.put(BOOST_PRESSURE_CONTROL, "Boost pressure control");
    dictionary.put(VARIABLE_GEOMETRY_TURBO_CONTROL, "Variable Geometry turbo control");
    dictionary.put(WASTEGATE_CONTROL, "Wastegate control");
    dictionary.put(EXHAUST_PRESSURE, "Exhaust pressure");
    dictionary.put(TURBOCHARGER_RPM, "Turbocharger RPM");
    dictionary.put(CHARGE_AIR_COOLER_TEMPERATURE, "Charge air cooler temperature");
    dictionary.put(EXHAUST_GAS_TEMPERATURE_BANK_1, "Exhaust Gas temperature Bank 1");
    dictionary.put(EXHAUST_GAS_TEMPERATURE_BANK_2, "Exhaust Gas temperature Bank 2");
    dictionary.put(DIESEL_PARTICULATE_FILTER_TEMPERATURE, "Diesel Particulate filter temperature");
    dictionary.put(NOX_NTE_CONTROL_AREA_STATUS, "NOx NTE control area status");
    dictionary.put(PM_NTE_CONTROL_AREA_STATUS, "PM NTE control area status");
    dictionary.put(ENGINE_RUN_TIME, "Engine run time");
    dictionary.put(PIDS_SUPPORTED_81_A0, "PIDs supported 81 A0");
    dictionary.put(NOX_SENSOR, "NOx sensor");
    dictionary.put(MANIFOLD_SURFACE_TEMPERATURE, "Manifold surface temperature");
    dictionary.put(NOX_REAGENT_SYSTEM, "NOx reagent system");
    dictionary.put(PARTICULATE_MATTER_SENSOR, "Particulate matter sensor");
    dictionary.put(PIDS_SUPPORTED_A1_C0, "PIDs supported A1 C0");
    dictionary.put(PIDS_SUPPORTED_C1_E0, "PIDs supported C1 E0");
    dictionary.put(STORED_DTCS, "Stored DTCs");
    dictionary.put(PENDING_DTCS, "DTCs detected in the last drive cycle");
    dictionary.put(PERMANENT_DTCS, "Permanent DTCs");
    dictionary.put(CLEAR_DTCS, "Clear DTCs and turn MIL off");
  }

  private final String pid;

  public Pid(String pid) {
    this.pid = pid.replace(" ", "").toLowerCase();
  }

  public Pid(int pid) {
    this.pid = String.format("%04x", pid & 0xFFFF);
  }

  public String getDescription() {
    return dictionary.get(this);
  }

  public String getPid() {
    return pid;
  }

  @Override
  public String toString() {
    return pid;
  }

  @Override
  public boolean equals(Object other) {
    if(this == other) {
      return true;
    }

    if(other instanceof Pid) {
      Pid otherPid = (Pid) other;
      return pid.equals(otherPid.getPid());
    }

    return false;
  }

  @Override
  public int hashCode() {
    return pid.hashCode();
  }
}
