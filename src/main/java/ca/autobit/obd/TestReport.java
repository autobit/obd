package ca.autobit.obd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestReport {
  private final Map<Pid, TestResult> cache;
  private final List<Pid> supportedPids;
  private final List<TestResult> tests;

  public TestReport(List<TestResult> tests) {
    this.tests = tests;
    this.cache = new HashMap<>();
    this.supportedPids = new ArrayList<>();

    for(TestResult test : tests) {
      cache.put(test.getPid(), test);

      if(test.isSupported()) {
        supportedPids.add(test.getPid());
      }
    }
  }

  public List<TestResult> getTests() {
    return tests;
  }

  public List<Pid> getSupportedPids() {
    return supportedPids;
  }

  public boolean supports(Pid pid) {
    return cache.containsKey(pid) && cache.get(pid).isSupported();
  }
}
