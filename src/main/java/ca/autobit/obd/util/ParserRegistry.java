package ca.autobit.obd.util;

import ca.autobit.obd.Pid;
import ca.autobit.obd.parser.*;

import java.util.HashMap;
import java.util.Map;

public class ParserRegistry {
  private static final Map<Pid, ResponseParser> parserMap = new HashMap<>();

  static {
    parserMap.put(Pid.FUEL_RAIL_ABSOLUTE_PRESSURE, new AbsoluteFuelRailPressureParser());
    parserMap.put(Pid.ACTUAL_TORQUE, new ActualTorqueParser());
    parserMap.put(Pid.BATTERY_VOLTAGE, new BatteryVoltageParser());
    parserMap.put(Pid.DEMAND_TORQUE, new DemandTorqueParser());
    parserMap.put(Pid.DISTANCE_TRAVELED_SINCE_CODES_CLEARED, new DistanceSinceCodesClearedParser());
    parserMap.put(Pid.ENGINE_COOLANT_TEMPERATURE, new EngineCoolantTemperatureParser());
    parserMap.put(Pid.CALCULATED_ENGINE_LOAD, new EngineLoadParser());
    parserMap.put(Pid.ENGINE_RPM, new EngineRpmParser());
    parserMap.put(Pid.ENGINE_RUN_TIME, new EngineRunTimeParser());
    parserMap.put(Pid.EVAPORATION_SYSTEM_VAPOR_PRESSURE, new EvaporationVaporPressureParser());
    parserMap.put(Pid.ENGINE_FUEL_RATE, new FuelInjectionRateParser());
    parserMap.put(Pid.FUEL_INJECTION_TIMING, new FuelInjectionTimingParser());
    parserMap.put(Pid.FUEL_TANK_LEVEL_INPUT, new FuelLevelInputParser());
    parserMap.put(Pid.FUEL_RAIL_PRESSURE, new FuelPressureParser());
    parserMap.put(Pid.FUEL_GAUGE_PRESSURE, new FuelPressureParser());
    parserMap.put(Pid.HYBRID_BATTERY_PACK_REMAINING_LIFE, new HybridBatteryRemainingLifeParser());
    parserMap.put(Pid.TIMING_ADVANCE, new IgnitionTimingAdvanceParser());
    parserMap.put(Pid.INTAKE_AIR_TEMPERATURE, new IntakeAirTemperatureParser());
    parserMap.put(Pid.INTAKE_MANIFOLD_ABSOLUTE_PRESSURE, new IntakeManifoldAbsolutePressureParser());
    parserMap.put(Pid.MAF_AIR_FLOW_RATE, new MassAirFlowRateParser());
    parserMap.put(Pid.MONITOR_STATUS_SINCE_DTCS_CLEARED, new MonitorStatusParser());
    parserMap.put(Pid.MONITOR_STATUS_THIS_DRIVE_CYCLE, new MonitorStatusParser());
    parserMap.put(Pid.ENGINE_OIL_TEMPERATURE, new OilTemperatureParser());
    parserMap.put(Pid.PENDING_DTCS, new PendingTroubleCodeParser());
    parserMap.put(Pid.PERMANENT_DTCS, new PermanentTroubleCodeParser());
    parserMap.put(Pid.ENGINE_REFERENCE_TORQUE, new ReferenceTorqueParser());
    parserMap.put(Pid.PIDS_SUPPORTED_01_20, new SupportedPidParser());
    parserMap.put(Pid.PIDS_SUPPORTED_21_40, new SupportedPidParser());
    parserMap.put(Pid.PIDS_SUPPORTED_41_60, new SupportedPidParser());
    parserMap.put(Pid.PIDS_SUPPORTED_61_80, new SupportedPidParser());
    parserMap.put(Pid.PIDS_SUPPORTED_81_A0, new SupportedPidParser());
    parserMap.put(Pid.PIDS_SUPPORTED_A1_C0, new SupportedPidParser());
    parserMap.put(Pid.PIDS_SUPPORTED_C1_E0, new SupportedPidParser());
    parserMap.put(Pid.THROTTLE_POSITION, new ThrottlePositionParser());
    parserMap.put(Pid.STORED_DTCS, new TroubleCodeParser());
    parserMap.put(Pid.VEHICLE_SPEED, new VehicleSpeedParser());
  }

  public static ResponseParser getParser(Pid requested) throws CannotFindParserException {
    if (!parserMap.containsKey(requested)) {
      throw new CannotFindParserException(requested);
    }

    return parserMap.get(requested);
  }
}
