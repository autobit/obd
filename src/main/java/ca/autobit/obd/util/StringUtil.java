package ca.autobit.obd.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
  public static final String NEW_LINE_PATTERN = "(\n)|(\r)|(\r\n)";
  private static StringUtil instance;

  private StringUtil() { }

  public static StringUtil getInstance() {
    if(instance == null) {
      instance = new StringUtil();
    }

    return instance;
  }

  /**
   * Credit: http://stackoverflow.com/a/18816371/2107333
   */
  public int getLineCount(String s) {
    if(s == null || s.isEmpty()) {
      return 0;
    }

    int lines = 1;
    Matcher m = Pattern.compile(NEW_LINE_PATTERN).matcher(s.trim());

    while (m.find()) {
      lines ++;
    }

    return lines;
  }

  public String stripNonAlphaNumeric(String s, boolean stripSpaces) {
    String pattern = stripSpaces ? "[^A-Za-z0-9]" : "[^A-Za-z0-9 ]";
    return s.replaceAll(pattern, "");
  }

  public String stripNonHex(String s, boolean stripSpaces) {
    String pattern = stripSpaces ? "[^a-fA-F0-9]" : "[^a-fA-F0-9 ]";
    return s.replaceAll(pattern, "");
  }

  public String stripNonNumeric(String s) {
    return s.replaceAll("[^0-9.]", "");
  }

  public String stripNewLines(String s) {
    return s.replace("\r", "").replace("\n", "");
  }
}
