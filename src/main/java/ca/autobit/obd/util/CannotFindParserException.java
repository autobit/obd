package ca.autobit.obd.util;

import ca.autobit.obd.Pid;

public class CannotFindParserException extends RuntimeException {
  public CannotFindParserException(Pid pid) {
    super(String.format("No Registered Parser for %s", pid.getPid()));
  }
}
